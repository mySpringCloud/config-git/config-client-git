package com.forezp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 验证刷新步骤：
 * 1、http://localhost:8002/hi  请求获取foo值
 * 2、更改git库里foo值
 * 3、http://localhost:8002/refresh 刷新某个微服务里的值，需类里加上注解@RefreshScope，并且jar依赖有spring-boot-starter-actuator
 * 4、http://localhost:8002/hi  获取新的foo值
 * 
 * @author zhuwen
 *
 */
@RestController
@RefreshScope
public class ConfigClientController {


	@Value("${foo}")
	String foo;

	@RequestMapping(value = "/hi")
	public String hi(){
		return foo;
	}
}
